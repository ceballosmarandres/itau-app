import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { Selection } from './src/Selection';
import { authentication } from './src/store/services/Firebase';
import { ActionSetSesion } from './src/store/actions/ActionSesion';

class App extends Component {
  componentDidMount() {
    this.props.authentication();
  }

  render() {
    return (
      <View style={styles.container}>
        <Selection user={this.props.user} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = state => ({
    user: state.ReducerSesion,
});

const mapDispatchToProps = dispatch => ({
  authentication: (isLoad) => {
    authentication.onAuthStateChanged((usuario) => {
      if (usuario) {
        dispatch(ActionSetSesion(usuario));
      }
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
