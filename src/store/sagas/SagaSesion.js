import { call, takeEvery, put } from 'redux-saga/effects';
import CONSTANTS from '../CONSTANTS';
import { showAlertError, showAlertSuccess } from '../../components/Alerts';
import { authentication } from '../services/Firebase';
import { ActionStopLoading } from '../actions/ActionApp';

const loginInFirebase = ({ email, password }) =>
authentication.signInWithEmailAndPassword(email, password).then(success => success);

const registerInFirebase = values =>
  authentication
  .createUserWithEmailAndPassword(values.email, values.password)
  .then(success => success);

function* Login(values) {
  try {
    yield call(loginInFirebase, values.data);
    yield put(ActionStopLoading());
  } catch (error) {
    console.log(error)
    showAlertError('errorGeneral')
    yield put(ActionStopLoading());
  }
}

function* register(values) {
  try {
    yield call(registerInFirebase, values.data);
    yield put(ActionStopLoading())
    showAlertSuccess('Se registro con exito!!') 
  } catch (error) {
    console.log('error', error)
  };
}

export const sagaSesion = [
  takeEvery(CONSTANTS.LOGIN, Login),
  takeEvery(CONSTANTS.REGISTER_USER, register),  
]

