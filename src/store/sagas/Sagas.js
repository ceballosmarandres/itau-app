import { all } from 'redux-saga/effects'
import { sagaSesion } from './SagaSesion'

export default function* rootSaga() {    
  yield all([
    ...sagaSesion,
  ])
};
