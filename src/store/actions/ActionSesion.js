import CONSTANTS from '../CONSTANTS';

export const ActionLogin = data => ({
    type: CONSTANTS.LOGIN,
    data,
});

export const ActionLogout = () => ({
    type: CONSTANTS.LOGOUT,
});

export const ActionSetSesion = usuario => ({
    type: CONSTANTS.SET_SESION,
    usuario,
});

export const ActionRegisterUser = data => ({
    type: CONSTANTS.REGISTER_USER,
    data
});

export const ActionSesion = (user) => ({
    type: CONSTANTS.SESION,
    user
});


export const ActionGetSesion = (user) => ({
    type: CONSTANTS.GET_SESION,
    user
});
