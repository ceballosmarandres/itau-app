
import { reducer as form } from 'redux-form'
import { combineReducers } from 'redux'
import { ReducerSesion } from './ReducerSesion'
import { ReducerLoading } from './ReducerApp'

export default(reducers = combineReducers({
    form,
    ReducerSesion,
    ReducerLoading,
}));