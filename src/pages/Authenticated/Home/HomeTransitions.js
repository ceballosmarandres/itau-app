import React from 'react';
import { FluidNavigator } from 'react-navigation-fluid-transitions';
import HomeScreen from './HomeScreen';
import ListsTodo from './ListsTodo';

const Navigator = FluidNavigator({
    home: { screen: HomeScreen },
    listTodo: { screen: ListsTodo },
});

class HomeTransitions extends React.Component {
    static router = Navigator.router;
    render() {
        const {navigation} = this.props;
        return (
            <Navigator navigation={navigation}/>
        );
    }
}

export default HomeTransitions;