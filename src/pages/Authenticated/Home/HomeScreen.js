import React, { Component } from 'react'
import { connect } from 'react-redux';
import { 
    Text, 
    View, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet } from 'react-native'
import { Transition} from 'react-navigation-fluid-transitions'
import { ButtonGeneral } from '../../../components/ButtonRegister';
import { ActionLogout } from '../../../store/actions/ActionSesion';
import { authentication } from '../../../store/services/Firebase';

let screenWidth = Dimensions.get('window').width;
let screenHeight = Dimensions.get('window').height;


class HomeScreen extends Component {

    static navigationOptions = {
        title: 'Home',
        headerTintColor: '#ffffff',
        headerStyle: {
            backgroundColor: '#2F95D6',
            borderBottomColor: '#ffffff',
            borderBottomWidth: 3,
        },
        headerTitleStyle: {
            fontSize: 18,
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedTapBarIndex: 0,
        };
    }
    
    render() {
        return (
                <View style={styles.bottomContainer}>
                    <TouchableOpacity
                    activeOpacity = {1}
                    onPress={() => {
                        this.props.navigation.navigate('listTodo', {item: 'Lista Todos' })
                    }}>
                        <Transition shared={'Lista Todos'}>
                            <View style={styles.bottomGridItemContainer}>
                                <Text style={styles.textLists}>Lista Todos</Text>
                            </View>
                        </Transition>
                    </TouchableOpacity>
                    <ButtonGeneral 
                    title="Cerrar sesion" 
                    click={() => this.props.closeSesion()}
                    color="#9c272c"
                    fontColor="white"
                    />
                </View>
        );
    }
}

const styles = StyleSheet.create({
    bottomContainer: {
        flex:1,
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: 'transparent'
    },
    bottomGridItemContainer: {
        marginLeft: 5,
        backgroundColor:'black',
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        width: screenWidth * 300 / 375,
        height: screenHeight / 2 - 70,
        flexDirection: 'column',
        justifyContent: 'center',
        borderRadius: 15
    },
    textLists: {
        marginLeft: 15,
        marginRight: 10,
        position: 'absolute',
        bottom: 20,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 35,
    }
});

const mapStateToProps = state => ({
    user : state.ReducerSesion && state.ReducerSesion.user ? state.ReducerSesion.user : false,
});

const mapDispatchToProps = dispatch => ({
    closeSesion: () => {
        authentication.signOut().then((response) => {
            dispatch(ActionLogout());
        })
    },
});
  
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);