import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    Text, 
    View, 
    TouchableOpacity, 
    Image, 
    Dimensions,
    StyleSheet,
    WebView,
    SafeAreaView } from 'react-native';
import { Transition} from 'react-navigation-fluid-transitions';
import { ActionSetLoading, ActionStopLoading } from '../../../store/actions/ActionApp';
import { LoadingSmall} from '../../../components/LoadingSmall';

let screenWidth = Dimensions.get('window').width;

class ListsTodo extends Component {

    render() {
        const { navigation,loading } = this.props;
        const item = navigation.getParam('item', '');
    
        const LoadingStatus = () => {
          if ( loading == 'true' )
            return <LoadingSmall />      
          return null;
        };

        return (
            <SafeAreaView style={styles.DetailMainContainer}>
                { LoadingStatus() }
                <Transition shared={item}>
                    <View style={styles.detailTopContainer}>
                        <View style={styles.navigationHeaderContainer}>
                            <TouchableOpacity 
                                onPress={() => {
                                    navigation.navigate('home')
                                }}>
                            <Image style={{width:30, height:30}}
                                source={require('../../../../assets/icons/down.png')}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.detailTopBottomSubContainer}>
                            <Text style={{
                                color: 'white',
                                fontWeight: 'bold',
                                fontSize: scaleToDimension(35),
                            }}>{item}</Text>
                        </View>
                    </View>
                </Transition>
                <WebView
                onLoadStart={() => (this.props.showLoading())}
                onLoad={() => this.props.hideLoading()}
                source={{uri: 'https://itau-4dd3b.firebaseapp.com'}}
                />              
            </SafeAreaView>
        );
    }
};

const scaleToDimension = (size) => {
    return screenWidth * size / 375
};

// All Styles related to design...
const styles = StyleSheet.create({
    DetailMainContainer: {
        flex: 1
    },
    detailTopContainer: {
        position:'relative',
        backgroundColor:'black',
        height: scaleToDimension(160),
        width: screenWidth,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius:30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,
        borderRadius:10
    },
    navigationHeaderContainer: {
        justifyContent: 'center',
        alignItems:'center'
    },
    detailTopBottomSubContainer: {
        width: screenWidth - 30,
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 15,
        left: 15,
        right: 15,
    },
    textLists:{
        color: 'white', 
        fontSize: scaleToDimension(15)
    }
});

const mapStateToProps = state => ({
    formSignIn: state.form.SignInForm,
    loading : state.ReducerLoading.loading
});
  
const mapDispatchToProps = dispatch => ({
  showLoading: () => {
    dispatch(ActionSetLoading());
  },
  hideLoading: () => {
    dispatch(ActionStopLoading());
  },
});
  
export default connect(mapStateToProps, mapDispatchToProps)(ListsTodo);