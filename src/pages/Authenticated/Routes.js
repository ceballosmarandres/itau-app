import { createStackNavigator } from 'react-navigation'
import HomeTransitions from './Home/HomeTransitions';

export const StackHome = createStackNavigator({
    home: {
      screen: HomeTransitions,
      navigationOptions: {
        header: null
      }
    }
},{
  headerMode:'none'
});
