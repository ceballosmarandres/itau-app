import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { ButtonRegister } from '../../../components/ButtonRegister';
import { fieldInput } from '../../../components/Fields';

const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = '*';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Email invalido';
  }

  if (!values.password) {
    errors.password = '*';
  } else if (values.password.length < 6) {
    errors.password = 'errorMin6';
  } else if (values.password.length > 15) {
    errors.password = 'errorMax6';
  }

  return errors;
};

const SignInForm = (props) => {
  return (
    <View style={styles.body}>
        <Field 
        name="email" 
        label="Correo electrónico" 
        keyboardType="email-address" 
        component={fieldInput} 
        ph="email@email.com" />
        <Field 
        name="password" 
        label="Contraseña" 
        component={fieldInput} 
        password={true} ph="******" />
        <View style={styles.button}>
            <ButtonRegister 
            title="Ingresar" 
            click={  props.login }                           
            invalid={ props.invalid } 
            color="black"
            />
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  body:{
    alignItems:'center'
  },
  button: {
      paddingTop:40
  }
});

export default reduxForm({
  form: 'SignInForm',
  validate,
})(SignInForm);