import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet, Platform, TouchableOpacity,SafeAreaView } from 'react-native';
import { ActionSetLoading } from '../../store/actions/ActionApp';
import { ActionLogin } from '../../store/actions/ActionSesion';
import SignInForm from './Forms/SignInForm';
import { LoadingSmall } from '../../components/LoadingSmall';

class SignIn extends Component {

  userLogin= () => {
    const values = this.props.formSignIn.values; 
    this.props.login(values);
  }

  render() {  
    const { loading } = this.props;
    
    const LoadingStatus = () => {
      if ( loading == 'true' )
        return <LoadingSmall />      
      return null;
    };

    return (
          <SafeAreaView style={styles.body} behavior="padding">
            {LoadingStatus()}
            <View style={styles.viewTitle}>
              <Text style={styles.textTitle}>Hola</Text>
              <Text style={styles.textTitle}>Inicia sesión!</Text>
            </View>
            <SignInForm login={this.userLogin}/>
            <View style={styles.viewRegister}>
              <Text>No estas registrado? </Text>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate('SignUp')}>
                <Text style={styles.textRegister}> Registrarme</Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
    body:{
      flex: 3,
      marginTop:100,
    },
    viewTitle: {
      paddingBottom:30
    },
    textTitle: {
      fontSize:30,
      marginLeft:35,
      fontFamily:'Ubuntu-Bold'
    },
    viewRegister:{
      flex:1,
      paddingTop:Platform.OS === 'ios' ? 0 : 50,
      flexDirection:'row',
      justifyContent:'center',
    },
    textRegister:{
      fontFamily:'Ubuntu-Bold',
      color:'red'
    }
})

const mapStateToProps = state => ({
    formSignIn: state.form.SignInForm,
    loading : state.ReducerLoading.loading
});
  
const mapDispatchToProps = dispatch => ({
  login: (data) => {
    dispatch(ActionSetLoading());
    dispatch(ActionLogin(data));
  },
});
  
export default connect(mapStateToProps, mapDispatchToProps)(SignIn);