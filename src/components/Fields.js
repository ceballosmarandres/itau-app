
import React from 'react';
import { View, Text, StyleSheet, TextInput, Platform } from 'react-native';

export const fieldInput = (props) => {
    return (
      <View style={styles.texInput}>
        <View style={styles.startRow}>
            <Text style={styles.inputNameText}>{props.label}</Text>  
        </View>
        <View style={styles.endRow}>
          <View style={styles.field}>
            <TextInput
              placeholder={props.ph}
              style={{fontSize:18, marginLeft: 10, marginRight:10}}
              editable={props.editable}
              keyboardType={props.keyboardType}
              onChangeText={props.input.onChange}
              textContentType={props.textContentType}
              value={props.input.value}
              // keyboardType={props.keyboardType ? props.keyboardType : 'default'}
              autoCapitalize="none"
              secureTextEntry={props.password}
              onBlur={props.input.onBlur}
            />
          </View>
          <View style={styles.linea} />
        {props.meta.touched &&
          props.meta.error && <Text style={styles.errors}>{props.meta.error}</Text>}
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
    texInput: {
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 15,
        marginBottom:5,
        marginTop:2,
        marginRight:15,
        //flex:1
    },
    startRow:{
        flex: 1,
        paddingBottom:Platform.OS === 'ios' ? 10 : 0,
        paddingTop:Platform.OS === 'ios' ? 0 : 10,
        justifyContent: 'center',
    },
    inputNameText:{
        textAlign: 'right', 
        alignSelf: 'stretch',
    }, 
    endRow:{
        flex:2, 
        marginLeft: 10,
    }, 
    field:{
        //flex:1,
        backgroundColor: 'rgba(200, 200, 200, 0.4)',
        height: 45,
        borderRadius:12,
        justifyContent: 'center',
        ...Platform.select({
            ios: {
              padding: 10,
              shadowColor: '#e8e6e6',
              shadowOffset: {
                width: 0,
                height: 3
              },
              shadowRadius: 5,
              shadowOpacity: 1.0
            },
            android: {
              elevation: 2,
            }
        })

    },
    linea: {
        height: 2,
    },
    errors: {
        color: '#FF0000',
    },
})