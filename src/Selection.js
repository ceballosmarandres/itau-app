
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { StackNoAuthenticated } from './pages/NoAuthenticated/StackNoAuthenticated';
import { StackHome } from './pages/Authenticated/Routes';

export const Selection = (props) => {
  return (
    <View style={styles.container}>
      { props.user !== null ? <StackHome /> : <StackNoAuthenticated /> }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});


